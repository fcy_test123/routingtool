/*
 * axis.h
 *
 *  Created on: 11 Oct 2021
 *      Author: cheeyong
 */

#pragma once

#include <boost/geometry/geometry.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <iostream>
#include <vector>
#include "lonlat.h"

#include "routingtool/axissegment.h"

namespace bg = boost::geometry;
namespace bgm = boost::geometry::model;

using std::string;
using std::vector;

namespace routingtool {

typedef bgm::linestring<LonLat> linestring_lonlat;
typedef bgm::box<LonLat> box_lonlat;

class Axis
{

public:
	// user defined constructor
	Axis(const linestring_lonlat& linestring, const string& axisName, const string& axisType)
	: m_linestring(linestring)
	, m_bbox(bg::return_envelope<box_lonlat> (m_linestring))
	, m_axisName(boost::algorithm::trim_copy(axisName))
	, m_axisType(boost::algorithm::trim_copy(axisType))
	{
		// Insert m_axisType to the g_axisTypeSet
		//GlobalVar::g_axisTypeSet.insert(m_axisType);
    	LonLat previousPt;
    	bool validPreviousPtFlag = false;
    	double totaldist = 0;
    	for (auto point : m_linestring) {
    		if (validPreviousPtFlag) {
    			m_axisSegments.push_back(AxisSegment{previousPt, point});
    		}
    		validPreviousPtFlag = true;
    		previousPt = point;
    	}

	};

	// default constructor
	Axis() = delete;

	// default destructor
	~Axis() = default;

	// move constructor
	Axis ( Axis&& ) = default;
	// move assignment
	Axis& operator= (Axis&&) = default;
	// copy constructor
	Axis (const Axis&) = default;
	// copy assignment
	Axis& operator= (const Axis&) = default;

	// overwrite stream insertion operator <<
	friend std::ostream &operator<<( std::ostream &output, const Axis& rhs ) {
    	output << "axisName, axisType : " << rhs.axisName() << ", " << rhs.axisType();
    	output << "axisSegment ";
    	for (AxisSegment axisSegment : rhs.m_axisSegments) {
    		output << axisSegment;
    	}
	    return output;
	}

// getters
	linestring_lonlat linestring() const { return m_linestring; }
	box_lonlat bbox() const { return m_bbox; }
	string axisName() const { return m_axisName; }
	string axisType() const { return m_axisType; }
	vector<AxisSegment> axisSegments() const { return m_axisSegments; }

// setters
    void setAxisName(const string& axisName) { m_axisName = axisName; }
    void setAxisType(const string& axisType) { m_axisType = axisType; }

private:
    linestring_lonlat m_linestring;
    box_lonlat m_bbox;
    string m_axisName;
    string m_axisType;
    vector<AxisSegment> m_axisSegments;

};

}
