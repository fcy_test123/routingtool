/*
 * axisgraph.h
 *
 *  Created on: 12 Oct 2021
 *      Author: cheeyong
 */

// https://stackoverflow.com/questions/34878147/boost-dijkstra-string-edge-weight
// https://stackoverflow.com/questions/48367649/boost-dijkstra-shortest-paths-cant-extract-or-find-the-path-path-contains
// https://stackoverflow.com/questions/67110765/using-boost-how-can-i-put-get-custom-edge-properties-as-a-struct
// https://stackoverflow.com/questions/33988534/size-changing-graph-using-boost-graph
// https://stackoverflow.com/questions/3100146/adding-custom-vertices-to-a-boost-graph
// https://stackoverflow.com/questions/29382402/boost-undirected-graph-merging-vertices

#pragma once

#include <boost/geometry/geometry.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/property_map/transform_value_property_map.hpp>

#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>
#include "lonlat.h"

#include "routingtool/axis.h"
#include "routingtool/axissegment.h"

using std::vector;
using std::unordered_map;
using std::string;

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

namespace routingtool {

struct AxisGraphEdge {
	string axisName;
	string axisType;
	unsigned int distanceMeter;

};

using routingGraph_t = boost::adjacency_list<
		boost::setS
		, boost::vecS
		, boost::undirectedS
		, routingtool::LonLat
		, boost::property<boost::edge_weight_t, routingtool::AxisGraphEdge> // interior property>
>;

using routingGraphVertex_t = boost::graph_traits<routingGraph_t>::vertex_descriptor;

using routingGraphEdge_t   = boost::graph_traits<routingGraph_t>::edge_descriptor;
// iterator NOT descriptor
using routingGraphEdgeItr_t   = boost::graph_traits<routingGraph_t>::edge_iterator;

using routingPath_t = vector<routingGraphVertex_t>;

// using packing algo for speed in loading
// https://stackoverflow.com/questions/40256361/serialize-de-serialize-boost-geometry-rtree
using routingTree_t = bgi::rtree<routingtool::LonLat, bgi::linear<32> >;

class AxisGraph {

public:
	// default constructor
	AxisGraph() = default;

	// default destructor
	~AxisGraph() = default;

	// move constructor
	AxisGraph ( AxisGraph&& ) = default;
	// move assignment
	AxisGraph& operator= (AxisGraph&&) = default;
	// copy constructor
	AxisGraph (const AxisGraph&) = default;
	// copy assignment
	AxisGraph& operator= (const AxisGraph&) = default;

	// business logic
	void addAxis(const Axis& axis) {
		m_axes.push_back(axis);
	}

	void generateGraph() {

		for (Axis axis : axes()) {
			for (AxisSegment axisSegment : axis.axisSegments()) {
				// check if lonlat is in the m_graphVertexMap
				routingGraphVertex_t vertexStartPt;
				auto findVertexStartPt = m_graphVertexMap.find(axisSegment.startPt());
				if ( findVertexStartPt == m_graphVertexMap.end() ) {
					// not found
					vertexStartPt = boost::add_vertex(axisSegment.startPt(), m_graph);
					std::pair<LonLat, routingGraphVertex_t> latlongVertexPair (axisSegment.startPt(),vertexStartPt);
					// copy insertion
					m_graphVertexMap.insert(latlongVertexPair);
				}
				else {
					vertexStartPt = findVertexStartPt->second;
				}
				routingGraphVertex_t vertexEndPt;
				auto findVertexEndPt = m_graphVertexMap.find(axisSegment.endPt());
				if ( findVertexEndPt == m_graphVertexMap.end() ) {
					// not found
					vertexEndPt = boost::add_vertex(axisSegment.endPt(), m_graph);
					std::pair<LonLat, routingGraphVertex_t> latlongVertexPair (axisSegment.endPt(),vertexEndPt);
					// copy insertion
					m_graphVertexMap.insert(latlongVertexPair);
				}
				else {
					vertexEndPt = findVertexEndPt->second;
				}

			    // Create an edge conecting those two vertices
			    boost::add_edge(vertexStartPt
			    		, vertexEndPt
						, AxisGraphEdge{axis.axisName(), axis.axisType(), axisSegment.distanceInMeter()}
						, m_graph);
			}
		}
		auto t0 = std::chrono::high_resolution_clock::now();
		// load m_tree faster
		// https://stackoverflow.com/questions/8483985/obtaining-list-of-keys-and-values-from-unordered-map
		auto key_selector = [](auto pair){return pair.first;};
		// vectors to hold keys
		std::vector<LonLat> keys(m_graphVertexMap.size());
		// This is the crucial bit: Transform map to list of keys (or values)
		std::transform(m_graphVertexMap.begin(), m_graphVertexMap.end(), keys.begin(), key_selector);
		//
		m_tree.insert(keys.begin(), keys.end());
		auto t1 = std::chrono::high_resolution_clock::now();
	    auto time_taken_milli = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
	    std::cout << "m_tree.insert time " << time_taken_milli.count() << "ms" << std::endl;


	}

	routingGraphVertex_t closest(LonLat const& where) const {
		LonLat closest[1];
        size_t n = bgi::query(m_tree, bgi::nearest(where, 1), closest);
        assert(1 == n);
        return m_graphVertexMap.at(*closest); // throws if missing m_graphVertexMap entry
    }

	routingPath_t findPath(LonLat const& startPos, LonLat const& goalPos) const {
        return findPath(closest(startPos), closest(goalPos));
    }

	routingPath_t findPath(routingGraphVertex_t start, routingGraphVertex_t goal) const {

        auto idmap = boost::get(boost::vertex_index, m_graph);

        std::vector<routingGraphVertex_t> predecessors(boost::num_vertices(m_graph), m_graph.null_vertex());
        std::vector<unsigned int> distances(boost::num_vertices(m_graph));

	    // lambda expression
	    auto edge2Weight = [](AxisGraphEdge const& edge) {
	    	return edge.distanceMeter;
	    };

	    boost::dijkstra_shortest_paths(
        	m_graph
        	, start
			, boost::weight_map  (boost::make_transform_value_property_map(edge2Weight, get(boost::edge_weight, m_graph)))
            .distance_map(boost::make_iterator_property_map(distances.begin(), idmap))
            .predecessor_map(boost::make_iterator_property_map(predecessors.begin(), idmap))
        );

        // extract path
	    routingGraphVertex_t current = goal;
	    routingPath_t path { current };
        do {
           auto const pred = predecessors.at(current);
           //std::cout << "extract path: " << current << " " << m_graph[current] << " <- " << pred << std::endl;
           //std::cout << "distance: " << distances.at(current) << std::endl;
           if(current == pred)
               break;
           current = pred;
           path.push_back(current);
        } while(current != start);

        std::reverse(path.begin(), path.end());

        return path;
    }

	void printRoutingPath(routingPath_t const& path) const {

		auto lonlatStart  = m_graph[*path.begin()]; // c++17
		auto lonlatGoal  = m_graph[*path.rbegin()]; // c++17

		std::cout << "start: " << std::to_string(lonlatStart.x()) << " " << std::to_string(lonlatStart.y()) << std::endl;
		std::cout << "goal : " << std::to_string(lonlatGoal.x())  << " " << std::to_string(lonlatGoal.y()) << std::endl;

	    routingGraphVertex_t prevVertice;
	    bool validPrevVertice = false;
	    for (auto vertice : path) {
	    	auto lonlat  = m_graph[vertice]; // c++17

	    	if (validPrevVertice) {
	    		auto prevlonlat  = m_graph[validPrevVertice];
	    		std::cout << std::to_string(lonlat.x()) << " " << std::to_string(lonlat.y())
	    			<< " to "
	    			<< std::to_string(prevlonlat.x()) << " " << std::to_string(prevlonlat.y()) << std::endl;
				std::pair < routingGraphEdge_t, bool > edgePair = boost::edge( prevVertice, vertice, m_graph );
				if(edgePair.second == true) {
					// edge exists!
					auto axisGraphEdge = get(boost::edge_weight_t(), m_graph, edgePair.first);
					std::cout << "via "
						<< axisGraphEdge.axisName
						<< " (" << axisGraphEdge.axisType
						<< ") " << std::to_string(axisGraphEdge.distanceMeter)
						<< " meters"
						<< std::endl;

				}
	    	}
	    	validPrevVertice = true;
	    	prevVertice = vertice;

	    }

	}

	void printGraph() {

		boost::print_graph(m_graph);
	}

	// getters
	vector<Axis> axes() const { return m_axes; }
	routingGraph_t graph() const { return m_graph; }


private:
	routingGraph_t m_graph;
	vector<Axis> m_axes;
	unordered_map<LonLat,routingGraphVertex_t,LatLonHasher> m_graphVertexMap;
	routingTree_t m_tree;
};

}
