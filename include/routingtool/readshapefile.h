/*
 * readshapefile.h
 *
 *  Created on: 7 Oct 2021
 *      Author: cheeyong
 */

// Boost.Geometry (aka GGL, Generic Geometry Library)
//
// Copyright (c) 2007-2012 Barend Gehrels, Amsterdam, the Netherlands.
// Copyright (c) 2008-2012 Bruno Lalande, Paris, France.
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//
// SHAPELIB example

// Shapelib is a well-known and often used library to read (and write) shapefiles by Frank Warmerdam

// To build and run this example:
// 1) download shapelib from http://shapelib.maptools.org/
// 2) extract and put the source "shpopen.cpp" in project or makefile
// 3) download a shapefile, for example world countries from http://aprsworld.net/gisdata/world
// Alternativelly, install Shapelib using OSGeo4W installer from http://trac.osgeo.org/osgeo4w/
// that provides Windows binary packages


#pragma once

#include <boost/geometry/geometry.hpp>

#include <string>
#include <vector>
#include <iostream>

#include "shapefil.h"
#include "routingtool/axis.h"

namespace routingtool {
template <typename F>
void readLineString_shapefile(const std::string& filename, std::vector<Axis>& axes, F functor)
{
    try
    {
    	std::string shpFilename = filename + ".shp";
    	std::string dbfFilename = filename + ".dbf";

        SHPHandle sphHandle = SHPOpen(shpFilename.c_str(), "rb");
        DBFHandle dbfHandle = DBFOpen(dbfFilename.c_str(), "rb");

        if (sphHandle <= 0 ||
        	dbfHandle <= 0	)
        {
            throw std::string("File " + filename + " not found");
        }

        int nShapeType, nEntities;
        double adfMinBound[4], adfMaxBound[4];
        SHPGetInfo(sphHandle, &nEntities, &nShapeType, adfMinBound, adfMaxBound );

        for (int i = 0; i < nEntities; i++)
        {
            SHPObject* psShape = SHPReadObject(sphHandle, i );


            int axisTypeNum = 2;
            int axisNameNum = 3;
            // 2 = type of road; service, tertiary_link, primary_link, primary
            // 3 = name of road; not all roads have names
            std::string axisType{DBFReadStringAttribute(dbfHandle, i, axisTypeNum)};
            std::string axisName{DBFReadStringAttribute(dbfHandle, i, axisNameNum)};
            //std::cout << "attributeString :" << attributeString << std::endl;

            // Read only SHPT_ARC
            if (psShape->nSHPType == SHPT_ARC)
            {
            	linestring_lonlat linestring;
                functor(psShape, linestring);
                Axis axis(linestring, axisName, axisType);
                axes.push_back(axis);
            }
            SHPDestroyObject( psShape );
        }
        SHPClose(sphHandle);
        DBFClose(dbfHandle);
    }
    catch(const std::string& s)
    {
        throw s;
    }
    catch(...)
    {
        throw std::string("Other exception");
    }
}

template <typename T>
void convert(SHPObject* psShape, T& linestring)
{
	double* x = psShape->padfX;
    double* y = psShape->padfY;
    for (int v = 0; v < psShape->nVertices; v++)
    {
        typename boost::geometry::point_type<T>::type point;
        boost::geometry::assign_values(point, x[v], y[v]);
        boost::geometry::append(linestring, point);
    }
}

}
