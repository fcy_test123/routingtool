/*
 * globalvar.h
 *
 *  Created on: 7 Oct 2021
 *      Author: cheeyong
 */
#pragma once

#include <string>
#include <unordered_set>

using std::string;
using std::unordered_set;

namespace routingtool {
// https://www.fluentcpp.com/2019/07/23/how-to-define-a-global-constant-in-cpp/
class GlobalVar
{
public:
	static inline string const S = "Forty-Two";

	// from wiki
	static inline double const EARTH_RADIUS_KM = 6378.1;
	// Creating an Unordered_set of type string
	// to store all available axis type from OSM shp file
	// for us to see what axisType available for us
	static inline unordered_set<string> g_axisTypeSet;

	// Creating an Unordered_set of type string
	// to store WHAT WE WANT axis type from OSM shp file
	static inline unordered_set<string> g_wantedAxisTypeSet {
		"motorway"
		, "motorway_link"
		//, "track_grade4"
		//, "track_grade5"
		//, "bridleway"
		//, "track_grade3"
		//, "cycleway"
		//, "steps"
		//, "unclassified"
		, "primary"
		//, "residential"
		//, "footway"
		, "secondary_link"
		//, "track"
		//, "service"
		, "tertiary_link"
		//, "path"
		, "secondary"
		//, "trunk_link"
		//, "trunk"
		//, "pedestrian"
		, "tertiary"
		, "primary_link"
		//, "track_grade1"
		//, "track_grade2"
		//, "living_street"
	};

};

}
