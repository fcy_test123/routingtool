/*
 * latlon.h
 *
 *  Created on: 7 Oct 2021
 *      Author: cheeyong
 */
#pragma once

#include <boost/functional/hash.hpp>
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <iostream>
#include <limits>

namespace bg = boost::geometry;

namespace routingtool {

// https://stackoverflow.com/questions/42182537/how-can-i-use-the-rtree-of-the-boost-library-in-c
class LonLat
{
public:
    explicit LonLat(double lon, double lat) : m_lon(lon),m_lat(lat){}

    // default constructor
    LonLat() = default;

	// default destructor
	~LonLat() = default;

	// move constructor
	LonLat ( LonLat&& ) = default;
	// move assignment
	LonLat& operator= (LonLat&&) = default;
	// copy constructor
	LonLat (const LonLat&) = default;
	// copy assignment
	LonLat& operator= (const LonLat&) = default;

	// equal operator, we need it for std::map or std::unordered_map
	// https://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key
	bool operator==(const LonLat& rhs) const{
        if ( rhs.lon()==this->lon() &&
        	rhs.lat()==this->lat()) {
            return true;
        } else {
            return false;
        }
    }

	// overwrite stream insertion operator <<
	friend std::ostream &operator<<( std::ostream &output, const LonLat& rhs ) {
		output << "m_lon, m_lat : " << rhs.m_lon << ", " << rhs.m_lat;
	    return output;
	}

// getters
	double lon() const { return m_lon; }
    double lat() const { return m_lat; }
    double x() const { return m_lon; }
    double y() const { return m_lat; }

// setters
    void setLon(double lon) { m_lon = lon; }
    void setLat(double lat) { m_lat = lat; }

private:
    double m_lon = std::numeric_limits<double>::quiet_NaN();
    double m_lat = std::numeric_limits<double>::quiet_NaN();
};


// equal operator, we need it for std::map or std::unordered_map
// https://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key
struct LatLonHasher
{
  std::size_t operator()(const LonLat& rhs) const noexcept
  {
      using boost::hash_value;
      using boost::hash_combine;

      // Start with a hash value of 0    .
      std::size_t seed = 0;

      // Modify 'seed' by XORing and bit-shifting in
      // one member of 'Key' after the other:
      hash_combine(seed,hash_value(rhs.lon()));
      hash_combine(seed,hash_value(rhs.lat()));

      // Return the result.
      return seed;
  }
};

}

BOOST_GEOMETRY_REGISTER_POINT_2D_GET_SET(routingtool::LonLat, double,
                                         bg::cs::spherical_equatorial<bg::degree>,
                                         lon, lat, setLon, setLat)
