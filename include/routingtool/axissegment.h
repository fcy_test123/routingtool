/*
 * axissegment.h
 *
 *  Created on: 12 Oct 2021
 *      Author: cheeyong
 */
#pragma once

#include <boost/geometry/geometry.hpp>
#include "lonlat.h"

#include "routingtool/globalvar.h"

namespace bg = boost::geometry;

namespace routingtool {

class AxisSegment
{
public:
	// user defined constructor
	AxisSegment(const LonLat& start, const LonLat& end)
	: m_start(start)
	, m_end(end)
	{
		double dist = bg::distance(m_start, m_end);
		double dist_km = dist * GlobalVar::EARTH_RADIUS_KM;
		m_distance_meter = dist_km * 1000;
	};

	// default constructor
	AxisSegment() = delete;

	// default destructor
	~AxisSegment() = default;

	// move constructor
	AxisSegment ( AxisSegment&& ) = default;
	// move assignment
	AxisSegment& operator= (AxisSegment&&) = default;
	// copy constructor
	AxisSegment (const AxisSegment&) = default;
	// copy assignment
	AxisSegment& operator= (const AxisSegment&) = default;

	// overwrite stream insertion operator <<
	friend std::ostream &operator<<( std::ostream &output, const AxisSegment& rhs ) {
    	output << "start : " << rhs.m_start;
    	output << "end : " << rhs.m_end;
    	output << "dist : " << rhs.m_distance_meter;
	    return output;
	}

// getters
	LonLat startPt() const { return m_start; }
	LonLat endPt() const { return m_end; }
	unsigned int distanceInMeter() const { return m_distance_meter; }


private:
    LonLat m_start;
    LonLat m_end;
    unsigned int m_distance_meter;
};

}
