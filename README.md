# routingtool

A C++ minimal working example to 

1) Construct a graph from Open Street Map road shp files

2) Find the shortest paths between 2 locations using Dijkstra's algorithm

## Required Libraries

http://shapelib.maptools.org/

The Shapefile C library provides the ability to write simple C programs for reading, writing and updating (to a limited extent) ESRI Shapefiles, and the associated attribute file (.dbf).

https://www.boost.org/

routingtool is tested with boost libraries version 1.77.0 b

## C++

C++17 compiler

## OpenStreetMap 

Routingtool lib has been tested with Singapore OpenStreetMap road shp files.
You can find them in resources/Sg_roads

Routingtool needs the xxx.shp for the geometry of the roads.
Routingtool needs the xxx.dbf for the details of the roads.
Currently routingtool reads in the road name and road types from the xxx.dbf file.

## Getting started

Routingtool is a header-only library, there are only 6 header files.

See main.cpp on how to run routingtool

Change the path to point to the shp files on your machine

```
std::string filename = "../../resources/Sg_roads/Sg_roads";
```

Do not include ".shp" or ".dbf" to the filename, routingtool will append ".shp" and ".dbf" to the filename.

There are 3 examples of finding the shortest path between start point and goal point.

## Sample Output

For example 3 in main.cpp

```
start: 103.839495 1.313585
goal : 103.841319 1.288699
103.839288 1.313432 to 103.696382 1.335165
via Newton Road (primary) 28 meters
103.839348 1.313414 to 103.696382 1.335165
via Newton Circus (primary) 6 meters
103.839445 1.313380 to 103.696382 1.335165
via Newton Circus (primary) 11 meters
103.839539 1.313327 to 103.696382 1.335165
via Newton Circus (primary) 11 meters
103.839588 1.313226 to 103.696382 1.335165
via Newton Circus (primary) 12 meters
...
...
via Buyong Road (tertiary) 25 meters
103.843146 1.299171 to 103.696382 1.335165
via Penang Road (primary) 19 meters
103.843367 1.299025 to 103.696382 1.335165
via Penang Road (primary) 29 meters
103.843644 1.298851 to 103.696382 1.335165
via Penang Road (primary) 36 meters
103.843663 1.298761 to 103.696382 1.335165
via Clemenceau Avenue (primary) 10 meters
103.843671 1.298577 to 103.696382 1.335165
via Clemenceau Avenue (primary) 20 meters
103.843689 1.298150 to 103.696382 1.335165
via Clemenceau Avenue (primary) 47 meters
...
...
103.841319 1.288699 to 103.696382 1.335165
via Clemenceau Avenue (primary) 36 meters
time 137ms

test 3 completed
```

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fcy_test123/routingtool.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:68b01f12351788eaec7acc6b9b8c9843?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

