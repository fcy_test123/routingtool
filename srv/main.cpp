#include <boost/geometry/geometry.hpp>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#include "routingtool/axis.h"
#include "routingtool/axisgraph.h"
#include "routingtool/globalvar.h"
#include "routingtool/readshapefile.h"
#include "routingtool/lonlat.h"

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

using namespace routingtool;

//void testRTree(std::vector<Axis> linestrings);

int main()
{
	std::string filename = "../../resources/Sg_roads/Sg_roads";

	std::vector<Axis> allAxes;

    try
    {
    	readLineString_shapefile(filename, allAxes, convert<linestring_lonlat>);
    }
    catch(const std::string& s)
    {
        std::cout << s << std::endl;
        return 1;
    }

    AxisGraph axisgraph;
    while ( !allAxes.empty()) {
    	auto axis = allAxes.back();
    	allAxes.pop_back();
        auto search = GlobalVar::g_wantedAxisTypeSet.find(axis.axisType());
        if (search != GlobalVar::g_wantedAxisTypeSet.end()) {
        	axisgraph.addAxis(axis);
        }
    }

    // print for trouble shooting
    //for (auto axis : axisgraph.axes()) {
    	//axis.print();
    //}

    auto t0 = std::chrono::high_resolution_clock::now();
    std::cout << "b4 generateGraph" << std::endl;
    std::cout << "takes ~30s on my PC" << std::endl;
    std::cout << "for bgi::rtree<routingtool::LonLat, bgi::rstar<32> > in axisgraph" << std::endl;
    std::cout << "takes ~7s on my PC" << std::endl;
    std::cout << "for bgi::rtree<routingtool::LonLat, bgi::linear<32> > in axisgraph" << std::endl;
    axisgraph.generateGraph();
    std::cout << "af generateGraph" << std::endl;
    auto t1 = std::chrono::high_resolution_clock::now();
    auto time_taken_milli = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "time " << time_taken_milli.count() << "ms" << std::endl;

    //axisgraph.printGraph();

    // example 1
    t0 = std::chrono::high_resolution_clock::now();
    LonLat start {103.802146, 1.272865};
    LonLat  goal {103.802261, 1.272774};
    auto path = axisgraph.findPath(start, goal);
    axisgraph.printRoutingPath(path);
    t1 = std::chrono::high_resolution_clock::now();
    time_taken_milli = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "time " << time_taken_milli.count() << "ms" << std::endl;
    std::cout << "test 1 completed" << std::endl;

    // example 2
    //axisName, axisType : Newton Road, primary
    t0 = std::chrono::high_resolution_clock::now();
    start = std::move(LonLat{103.839495, 1.313585});
    goal  = std::move(LonLat{103.840323, 1.314261});
    path = axisgraph.findPath(start, goal);
    axisgraph.printRoutingPath(path);
    t1 = std::chrono::high_resolution_clock::now();
    time_taken_milli = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "time " << time_taken_milli.count() << "ms" << std::endl;
    std::cout << "test 2 completed" << std::endl;

    // example 3
    //start axisName, axisType : Newton Road, primary
    //end axisName, axisType : Clemenceau Avenue, primary
    t0 = std::chrono::high_resolution_clock::now();
    start = std::move(LonLat{103.839495, 1.313585});
    goal  = std::move(LonLat{103.841319, 1.288699});
    path = axisgraph.findPath(start, goal);
    axisgraph.printRoutingPath(path);
    t1 = std::chrono::high_resolution_clock::now();
    time_taken_milli = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "time " << time_taken_milli.count() << "ms" << std::endl;
    std::cout << "test 3 completed" << std::endl;

    return 0;
}
